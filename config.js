require("dotenv").config();

module.exports = {
  mongoDB: process.env.MONGO_URL,
  secretKey: process.env.SECRET_KEY,
};
