const { ApolloServer } = require("apollo-server");
const mongoose = require("mongoose");
const { mongoDB } = require("./config");
const typeDefs = require("./graphql/typeDefs");
const resolvers = require("./graphql/resolvers");
const { PubSub } = require("graphql-subscriptions");
const {
  ApolloServerPluginLandingPageGraphQLPlayground,
} = require("apollo-server-core");

const pubsub = new PubSub();
const PORT = process.env.PORT || 5000;

const server = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  introspection: true,
  playground: true,
  context: ({ req }) => ({ req, pubsub }),
});

mongoose
  .connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected successfully to MongoDB");
    return server.listen({ port: PORT });
  })
  .then(({ url }) => {
    console.log(`Server is successfully running on ${url}`);
  })
  .catch((err) => {
    console.log("Error while connecting to mongoDB", err);
  });
