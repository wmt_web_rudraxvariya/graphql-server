const { gql } = require("apollo-server");

module.exports = gql`
  type Posts {
    id: ID!
    body: String!
    username: String!
    created_at: String!
    comments: [Comment]!
    likes: [Like]!
    likeCount: Int!
    commentCount: Int!
  }
  type Comment {
    id: ID!
    body: String!
    username: String!
    created_at: String!
    post: Posts!
  }
  type Like {
    id: ID!
    username: String!
    created_at: String!
    post: Posts!
  }

  type Users {
    username: ID!
    password: String!
    email: String!
    created_at: String!
  }

  input RegisterInput {
    username: String!
    password: String!
    confirmPassword: String!
    email: String!
  }
  type User {
    id: ID!
    email: String!
    username: String!
    token: String!
    created_at: String!
  }
  type Query {
    getPosts: [Posts]
    getUsers: [Users]
    getPost(postId: ID!): Posts
  }
  type Mutation {
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
    createPost(body: String!): Posts!
    deletePost(postId: ID!): String!
    createComment(postId: ID!, body: String!): Posts!
    deleteComment(postId: ID!, commentId: ID!): Posts!
    likePost(postId: ID!): Posts!
  }
  type Subscription {
    newPost: Posts!
  }
`;
