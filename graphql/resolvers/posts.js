const { AuthenticationError } = require("apollo-server");
const Post = require("../../models/Post");
const checkAuth = require("../../util/check-auth");

module.exports = {
  Query: {
    async getPosts() {
      try {
        const Posts = await Post.find();
        return Posts;
      } catch (err) {
        throw new Error(err);
      }
    },
    async getPost(_, { postId }) {
      try {
        const RPost = await Post.findById(postId).sort({ created_at: -1 });
        if (RPost) {
          return RPost;
        } else {
          throw new Error("post Not Found");
        }
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Mutation: {
    async createPost(_, { body }, context) {
      const user = checkAuth(context);
      if (body.trim() === "") {
        throw new Error("Post body can't be empty");
      }
      const newPost = new Post({
        body,
        user: user.id,
        username: user.username,
        created_at: new Date().toISOString(),
      });
      const RPost = await newPost.save();
      context.pubsub.publish("NEW_POST", {
        newPost: RPost,
      });
      return RPost;
    },
    async deletePost(_, { postId }, context) {
      const user = checkAuth(context);
      try {
        const post = await Post.findById(postId);
        if (user.username == post.username) {
          await post.delete();
          return "Post Deleted";
        } else {
          throw new AuthenticationError(
            "You are not authorized to delete this post"
          );
        }
      } catch (err) {
        throw new Error(err);
      }
    },
    async likePost(_, { postId }, context) {
      const { username } = checkAuth(context);

      const post = await Post.findById(postId);
      if (post) {
        if (post.likes.find((like) => like.username === username)) {
          // Post already likes, unlike it
          post.likes = post.likes.filter((like) => like.username !== username);
        } else {
          // Not liked, like post
          post.likes.push({
            username,
            created_at: new Date().toISOString(),
          });
        }

        await post.save();
        return post;
      } else throw new UserInputError("Post not found");
    },
  },
  Subscription: {
    newPost: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator("NEW_POST"),
    },
  },
};
