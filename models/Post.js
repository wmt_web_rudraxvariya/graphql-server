const { Schema, model } = require("mongoose");

const postSchema = new Schema({
  body: String,
  username: String,
  created_at: String,
  comments: [
    {
      body: String,
      username: String,
      created_at: String,
    },
  ],
  likes: [{ username: String, created_at: String }],
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
});

module.exports = model("Post", postSchema);
